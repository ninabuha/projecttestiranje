/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import model.Transaction;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author User
 */
public class TransactionDAOTest {
    
    public TransactionDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of convertFromResultSet method, of class TransactionDAO.
     */
    @Test
    public void testConvertFromResultSet() throws SQLException, ParseException {
        Date now = new Date();
        System.out.println("convertFromResultSet");
        ResultSet rs = mock(ResultSet.class);
        when(rs.getInt("id")).thenReturn(11);
        when(rs.getFloat("total_price")).thenReturn(20.0f);
        when(rs.getInt("amount")).thenReturn(1);
        when(rs.getDate("date")).thenReturn(new java.sql.Date(now.getTime()));
        when(rs.getInt("client_id")).thenReturn(1);
        when(rs.getInt("shop_item_id")).thenReturn(12);
        when(rs.getInt("delivery_service_id")).thenReturn(3);
        when(rs.getFloat("distance")).thenReturn(30.0f);
        TransactionDAO instance = new TransactionDAO();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-dd");
	String dateInString = "2020-01-02";
	Date date = sdf.parse(dateInString);
        Transaction expResult = new Transaction(11, 20, 1, date, 1, 12, 3, 30);
        Transaction result = instance.convertFromResultSet(rs);
        
        assertEquals(expResult.getId(), result.getId());
        assertEquals(expResult.getTotalPrice(), result.getTotalPrice(), 0.1);
        assertEquals(expResult.getAmount(), result.getAmount());
        assertEquals(expResult.getClientId(), result.getClientId());
        assertEquals(expResult.getShopItemId(), result.getShopItemId());
        assertEquals(expResult.getDeliveryServiceId(), result.getDeliveryServiceId());
        assertEquals(expResult.getDistance(), result.getDistance(), 0.1);
    }

    /**
     * Test of insertOne method, of class TransactionDAO.
     */
    @Test
    public void testInsertOne() {
        System.out.println("insertOne");
        Date now = new Date();
        Transaction object = new Transaction(null, 200, 10, now, 2, 1, 3, 300);
        TransactionDAO instance = new TransactionDAO();
        
        int actualRowNum = 0;
        ArrayList resultAll = instance.getAll();
        for(Object t: resultAll){
            actualRowNum++;
        }
        
        boolean expResult = true;
        boolean result = instance.insertOne(object);
        assertEquals(expResult, result);
        
        resultAll = instance.getAll();
        assertEquals(actualRowNum+1, resultAll.size());
        
        
        Transaction lastObject = (Transaction) resultAll.get(resultAll.size()-1);
        assertEquals(object.getTotalPrice(), lastObject.getTotalPrice(), 0.1);
        assertEquals(object.getAmount(), lastObject.getAmount());
        //assertEquals(new java.sql.Date(now.getTime()), lastObject.getDate());
        assertEquals(object.getClientId(), lastObject.getClientId());
        assertEquals(object.getShopItemId(), lastObject.getShopItemId());
        assertEquals(object.getDeliveryServiceId(), lastObject.getDeliveryServiceId());
        assertEquals(object.getDistance(), lastObject.getDistance(), 0.1);
    }
    
    // GRESKA
    @Test
    public void testInsertOneTInvalidPriceAmount() {
        System.out.println("insertOne");
        Transaction object = new Transaction(null, -200, -10, new Date(), 2, 1, 3, 300);
        TransactionDAO instance = new TransactionDAO();
        
        int actualRowNum = 0;
        ArrayList resultAll = instance.getAll();
        for(Object t: resultAll){
            actualRowNum++;
        }
        
        boolean expResult = false;
        boolean result = instance.insertOne(object);
        assertEquals(expResult, result);
        
        resultAll = instance.getAll();
        assertEquals(actualRowNum, resultAll.size());
        
        Transaction lastObject = (Transaction) resultAll.get(resultAll.size()-1);
        assertEquals(101379, lastObject.getTotalPrice(), 0.1);
        assertEquals(50, lastObject.getAmount());
        assertEquals("2020-01-12", lastObject.getDate());
        assertEquals(1, lastObject.getClientId());
        assertEquals(1, lastObject.getShopItemId());
        assertEquals(1, lastObject.getDeliveryServiceId());
        assertEquals(10, lastObject.getDistance(), 0.1);
    }
    
    @Test
    public void testInsertOneInvalidIds() {
        System.out.println("insertOne");
        Transaction object = new Transaction(null, 100, 15, new Date(), 215, 133, 324, 300);
        TransactionDAO instance = new TransactionDAO();
        
        int actualRowNum = 0;
        ArrayList resultAll = instance.getAll();
        for(Object t: resultAll){
            actualRowNum++;
        }
        
        boolean expResult = false;
        boolean result = instance.insertOne(object);
        assertEquals(expResult, result);
        
        resultAll = instance.getAll();
        assertEquals(actualRowNum, resultAll.size());
    }
    
    // GRESKA
    @Test
    public void testInsertOneTInvalidDistance() {
        System.out.println("insertOne");
        Transaction object = new Transaction(null, 200, 10, new Date(), 2, 1, 3, -300);
        TransactionDAO instance = new TransactionDAO();
        
        int actualRowNum = 0;
        ArrayList resultAll = instance.getAll();
        for(Object t: resultAll){
            actualRowNum++;
        }
        
        boolean expResult = false;
        boolean result = instance.insertOne(object);
        assertEquals(expResult, result);
        
        resultAll = instance.getAll();
        assertEquals(actualRowNum, resultAll.size());
        
        Transaction lastObject = (Transaction) resultAll.get(resultAll.size()-1);
        assertEquals(101379, lastObject.getTotalPrice(), 0.1);
        assertEquals(50, lastObject.getAmount());
        assertEquals("2020-01-12", lastObject.getDate().toString());
        assertEquals(1, lastObject.getClientId());
        assertEquals(1, lastObject.getShopItemId());
        assertEquals(1, lastObject.getDeliveryServiceId());
        assertEquals(10, lastObject.getDistance(), 0.1);
    }
    
}
