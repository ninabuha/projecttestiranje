/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Client;
import model.Transaction;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.ArgumentCaptor;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author User
 */
public class ClientDAOTest {
    
    public ClientDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of convertFromResultSet method, of class ClientDAO.
     */
    @Test
    public void testConvertFromResultSet() throws SQLException {
        System.out.println("convertFromResultSet");
        ResultSet rs = mock(ResultSet.class);
        when(rs.getInt("id")).thenReturn(1);
        when(rs.getString("name")).thenReturn("nina");
        when(rs.getString("username")).thenReturn("nina");
        when(rs.getString("password")).thenReturn("buha");
        ClientDAO instance = new ClientDAO();
        Client expResult = new Client(1, "nina", "nina", "buha");
        Client result = instance.convertFromResultSet(rs);
        
        assertEquals(expResult.getId(), result.getId());
        assertEquals(expResult.getName(), result.getName());
        assertEquals(expResult.getUsername(), result.getUsername());
        assertEquals(expResult.getPassword(), result.getPassword());
    }

    /**
     * Test of updateOne method, of class ClientDAO.
     */
    @Test
    public void testUpdateOne() {
        System.out.println("updateOne");
        Client object = new Client(1, "ninaa", "ninav", "buhamm");
        ClientDAO instance = new ClientDAO();
        boolean expResult = true;
        boolean result = instance.updateOne(object);
        assertEquals(expResult, result);
        
        Client checkObject = instance.getOne(object.getId());
        assertEquals(object.getName(), checkObject.getName());
        assertEquals(object.getUsername(), checkObject.getUsername());
        assertEquals(object.getPassword(), checkObject.getPassword());
    }
    
    // GRESKA
    @Test
    public void testUpdateOneClientInvalidId() {
        System.out.println("updateOne");
        Client object = new Client(1200, "xx", "ffg", "ffff");
        ClientDAO instance = new ClientDAO();
        boolean expResult = false;
        boolean result = instance.updateOne(object);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testUpdateOneNullId() {
        System.out.println("updateOne");
        Client object = new Client(null, "xx", "ffg", "ffff");
        ClientDAO instance = new ClientDAO();
        boolean expResult = false;
        boolean result = instance.updateOne(object);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testUpdateOneInvalidUsernamePass() {
        System.out.println("updateOne");
        
        ClientDAO instance = new ClientDAO();
        Client object = instance.getOne(1);
        Client updatedObject = new Client(object.getId(), "name", "", "");
        boolean expResult = false;
        boolean result = instance.updateOne(updatedObject);
        assertEquals(expResult, result);
        
        Client checkObject = instance.getOne(object.getId());
        assertEquals(object.getName(), checkObject.getName());
        assertEquals(object.getUsername(), checkObject.getUsername());
        assertEquals(object.getPassword(), checkObject.getPassword());
    }

    /**
     * Test of insertOne method, of class ClientDAO.
     */
    
    @Test
    public void testInsertOne() {
        System.out.println("insertOne");
        Client object = new Client(null, "zzz", "huhuh", "marlkooic");
        ClientDAO instance = new ClientDAO();
        
        int actualRowNum = 0;
        ArrayList resultAll = instance.getAll();
        for(Object t: resultAll){
            actualRowNum++;
        }
        
        boolean expResult = true;
        boolean result = instance.insertOne(object);
        assertEquals(expResult, result);
        
        resultAll = instance.getAll();
        assertEquals(actualRowNum+1, resultAll.size());
        
        Client lastObject = (Client) resultAll.get(resultAll.size()-1);
        assertEquals(object.getUsername(), lastObject.getUsername());
        assertEquals(object.getName(), lastObject.getName());
        assertEquals(object.getPassword(), lastObject.getPassword());
    }

    @Test
    public void testInsertOneEmptyUsPass() {
        System.out.println("insertOne");
        Client object = new Client(null, "kkkk", "", "");
        ClientDAO instance = new ClientDAO();
        
        int actualRowNum = 0;
        ArrayList resultAll = instance.getAll();
        for(Object t: resultAll){
            actualRowNum++;
        }
        
        boolean expResult = false;
        boolean result = instance.insertOne(object);
        assertEquals(expResult, result);
        
        resultAll = instance.getAll();
        assertEquals(actualRowNum, resultAll.size());
    }
    
}
