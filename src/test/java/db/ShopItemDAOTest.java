/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.ShopItem;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author User
 */
public class ShopItemDAOTest {
    
    public ShopItemDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of convertFromResultSet method, of class ShopItemDAO.
     */
    @Test
    public void testConvertFromResultSet() throws SQLException {
        System.out.println("convertFromResultSet");
        ResultSet rs = mock(ResultSet.class);
        when(rs.getInt("id")).thenReturn(1);
        when(rs.getString("name")).thenReturn("majica");
        when(rs.getFloat("price")).thenReturn(1200.0f);
        when(rs.getInt("amount")).thenReturn(250);
        ShopItemDAO instance = new ShopItemDAO();
        ShopItem expResult = new ShopItem(1, "majica", 1200, 250);
        ShopItem result = instance.convertFromResultSet(rs);
        
        assertEquals(expResult.getId(), result.getId());
        assertEquals(expResult.getName(), result.getName());
        assertEquals(expResult.getPrice(), result.getPrice(), 0.1);
        assertEquals(expResult.getAmount(), result.getAmount());
    }

    /**
     * Test of updateOne method, of class ShopItemDAO.
     */
    @Test
    public void testUpdateOne() {
        System.out.println("updateOne");
        ShopItem object = new ShopItem(1, "majica11", 1201, 251);
        ShopItemDAO instance = new ShopItemDAO();
        boolean expResult = true;
        boolean result = instance.updateOne(object);
        assertEquals(expResult, result);
        
        ShopItem checkObject = instance.getOne(object.getId());
        assertEquals(object.getName(), checkObject.getName());
        assertEquals(object.getPrice(), checkObject.getPrice(), 0.1);
        assertEquals(object.getAmount(), checkObject.getAmount());
    }
    
    // GRESKA
    @Test
    public void testUpdateOneSIInvaliedId() {
        System.out.println("updateOne");
        ShopItem object = new ShopItem(120, "ffff", 1201, 251);
        ShopItemDAO instance = new ShopItemDAO();
        boolean expResult = false;
        boolean result = instance.updateOne(object);
        assertEquals(expResult, result);
    }
    
    // GRESKA
    @Test
    public void testUpdateOneInvaliedNamePriceAmount() {
        System.out.println("updateOne");
        ShopItem object = new ShopItem(2, "", -5, -10);
        ShopItemDAO instance = new ShopItemDAO();
        boolean expResult = false;
        boolean result = instance.updateOne(object);
        assertEquals(expResult, result);
        
        ShopItem checkObject = instance.getOne(object.getId());
        assertEquals("someNewNaeme", checkObject.getName());
        assertEquals(350, checkObject.getPrice(), 0.1);
        assertEquals(300, checkObject.getAmount());
    }
    
    @Test
    public void testUpdateOnePriceAmountZero() {
        System.out.println("updateOne");
        ShopItem object = new ShopItem(2, "someNewNaeme", 0, 0);
        ShopItemDAO instance = new ShopItemDAO();
        boolean expResult = true;
        boolean result = instance.updateOne(object);
        assertEquals(expResult, result);
        
        ShopItem checkObject = instance.getOne(object.getId());
        assertEquals(object.getName(), checkObject.getName());
        assertEquals(object.getPrice(), checkObject.getPrice(), 0.1);
        assertEquals(object.getAmount(), checkObject.getAmount());
    }

    
    
    /**
     * Test of insertOne method, of class ShopItemDAO.
     */
    @Test
    public void testInsertOne() {
        System.out.println("insertOne");
        ShopItem object = new ShopItem(0, "InsertingNewitem", 140, 260);
        ShopItemDAO instance = new ShopItemDAO();
        
        int actualRowNum = 0;
        ArrayList resultAll = instance.getAll();
        for(Object t: resultAll){
            actualRowNum++;
        }
        
        boolean expResult = true;
        boolean result = instance.insertOne(object);
        assertEquals(expResult, result);
        
        resultAll = instance.getAll();
        assertEquals(actualRowNum+1, resultAll.size());
        
        ShopItem lastObject = (ShopItem) resultAll.get(resultAll.size()-1);
        assertEquals(object.getPrice(), lastObject.getPrice(), 0.1);
        assertEquals(object.getName(), lastObject.getName());
        assertEquals(object.getAmount(), lastObject.getAmount());
    }
    
    @Test
    public void testInsertOne1() {
        System.out.println("insertOne");
        ShopItem object = new ShopItem(0, "InsertingNewitem1", 0, 0);
        ShopItemDAO instance = new ShopItemDAO();
        
        int actualRowNum = 0;
        ArrayList resultAll = instance.getAll();
        for(Object t: resultAll){
            actualRowNum++;
        }
        
        boolean expResult = true;
        boolean result = instance.insertOne(object);
        assertEquals(expResult, result);
        
        resultAll = instance.getAll();
        assertEquals(actualRowNum+1, resultAll.size());
        
        ShopItem lastObject = (ShopItem) resultAll.get(resultAll.size()-1);
        assertEquals(object.getPrice(), lastObject.getPrice(), 0.1);
        assertEquals(object.getName(), lastObject.getName());
        assertEquals(object.getAmount(), lastObject.getAmount());
    }
    
    // GRESKA
    @Test
    public void testInsertOneinvalidNamePriceAmount() {
        System.out.println("insertOne");
        ShopItem object = new ShopItem(0, "", -100, -200);
        ShopItemDAO instance = new ShopItemDAO();
        
        int actualRowNum = 0;
        ArrayList resultAll = instance.getAll();
        for(Object t: resultAll){
            actualRowNum++;
        }
        
        boolean expResult = false;
        boolean result = instance.insertOne(object);
        assertEquals(expResult, result);
        
        resultAll = instance.getAll();
        assertEquals(actualRowNum, resultAll.size());
        
        ShopItem lastObject = (ShopItem) resultAll.get(resultAll.size()-1);
        assertEquals(200, lastObject.getPrice(), 0.1);
        assertEquals("NewItem", lastObject.getName());
        assertEquals(400, lastObject.getAmount());
    }
    
}
