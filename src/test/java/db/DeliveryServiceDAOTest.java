/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.DeliveryService;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author User
 */
public class DeliveryServiceDAOTest {
    
    public DeliveryServiceDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of convertFromResultSet method, of class DeliveryServiceDAO.
     */
    @Test
    public void testConvertFromResultSet() throws SQLException {
        System.out.println("convertFromResultSet");
        ResultSet rs = mock(ResultSet.class);
        when(rs.getInt("id")).thenReturn(1);
        when(rs.getString("name")).thenReturn("del1");
        when(rs.getFloat("starting_price")).thenReturn(100.0f);
        when(rs.getFloat("price_per_kilometer")).thenReturn(100.0f);
        DeliveryServiceDAO instance = new DeliveryServiceDAO();
        DeliveryService expResult = new DeliveryService(1, "del1", 100, 100);
        DeliveryService result = instance.convertFromResultSet(rs);
        
        assertEquals(expResult.getId(), result.getId());
        assertEquals(expResult.getName(), result.getName());
        assertEquals(expResult.getStartingPrice(), result.getStartingPrice(), 0.1);
        assertEquals(expResult.getPricePerKilometer(), result.getPricePerKilometer(), 0.1);
    }

    /**
     * Test of updateOne method, of class DeliveryServiceDAO.
     */
    @Test
    public void testUpdateOne() {
        System.out.println("updateOne");
        DeliveryService object = new DeliveryService(1, "changedDel1", 544, 520);
        DeliveryServiceDAO instance = new DeliveryServiceDAO();
        boolean expResult = true;
        boolean result = instance.updateOne(object);
        assertEquals(expResult, result);
        
        DeliveryService checkObject = instance.getOne(object.getId());
        assertEquals(object.getName(), checkObject.getName());
        assertEquals(object.getStartingPrice(), checkObject.getStartingPrice(), 0.1);
        assertEquals(object.getPricePerKilometer(), checkObject.getPricePerKilometer(), 0.1);
    }
    
    // GRESKA
    @Test
    public void testUpdateOneDSInvalidId() {
        System.out.println("updateOne");
        DeliveryService object = new DeliveryService(120, "invalidId", 145, 900);
        DeliveryServiceDAO instance = new DeliveryServiceDAO();
        boolean expResult = false;
        boolean result = instance.updateOne(object);
        assertEquals(expResult, result);
    }
    
    //GRESKA
    @Test
    public void testUpdateOneDSInvalidPrices() {
        System.out.println("updateOne");
        DeliveryService object = new DeliveryService(2, "vvv", 0, 0);
        DeliveryServiceDAO instance = new DeliveryServiceDAO();
        boolean expResult = false;
        boolean result = instance.updateOne(object);
        assertEquals(expResult, result);
        
        DeliveryService checkObject = instance.getOne(object.getId());
        assertEquals("del2", checkObject.getName());
        assertEquals(120, checkObject.getStartingPrice(), 0.1);
        assertEquals(250, checkObject.getPricePerKilometer(), 0.1);
    }
    
    //GRESKA
    @Test
    public void testUpdateOneDSInvalidName() {
        System.out.println("updateOne");
        DeliveryService object = new DeliveryService(2, "", 50, 450);
        DeliveryServiceDAO instance = new DeliveryServiceDAO();
        boolean expResult = false;
        boolean result = instance.updateOne(object);
        assertEquals(expResult, result);
        
        DeliveryService checkObject = instance.getOne(object.getId());
        assertEquals("del2", checkObject.getName());
        assertEquals(120, checkObject.getStartingPrice(), 0.1);
        assertEquals(250, checkObject.getPricePerKilometer(), 0.1);
    }

    /**
     * Test of insertOne method, of class DeliveryServiceDAO.
     */
    @Test
    public void testInsertOne() {
        System.out.println("insertOne");
        DeliveryService object = new DeliveryService(null, "newNewDel", 360, 250);
        DeliveryServiceDAO instance = new DeliveryServiceDAO();
        
        int actualRowNum = 0;
        ArrayList resultAll = instance.getAll();
        for(Object t: resultAll){
            actualRowNum++;
        }
        
        boolean expResult = true;
        boolean result = instance.insertOne(object);
        assertEquals(expResult, result);
        
        resultAll = instance.getAll();
        assertEquals(actualRowNum+1, resultAll.size());
        
        DeliveryService lastObject = (DeliveryService) resultAll.get(resultAll.size()-1);
        assertEquals(object.getStartingPrice(), lastObject.getStartingPrice(), 0.1);
        assertEquals(object.getName(), lastObject.getName());
        assertEquals(object.getPricePerKilometer(), lastObject.getPricePerKilometer(), 0.1);
    }
    
    //GRESKA
    @Test
    public void testInsertOneDSInvalidPrices() {
        System.out.println("insertOne");
        DeliveryService object = new DeliveryService(null, "newInvalidPrice", 0, 0);
        DeliveryServiceDAO instance = new DeliveryServiceDAO();
        
        int actualRowNum = 0;
        ArrayList resultAll = instance.getAll();
        for(Object t: resultAll){
            actualRowNum++;
        }
        
        boolean expResult = false;
        boolean result = instance.insertOne(object);
        assertEquals(expResult, result);
        
        resultAll = instance.getAll();
        assertEquals(actualRowNum, resultAll.size());
        
        DeliveryService lastObject = (DeliveryService) resultAll.get(resultAll.size()-1);
        assertEquals(470, lastObject.getStartingPrice(), 0.1);
        assertEquals("delNew", lastObject.getName());
        assertEquals(250, lastObject.getPricePerKilometer(), 0.1);
    }
    
    //GRESKA
    @Test
    public void testInsertOneDSInvalidName() {
        System.out.println("insertOne");
        DeliveryService object = new DeliveryService(null, "", 450, 30);
        DeliveryServiceDAO instance = new DeliveryServiceDAO();
        
        int actualRowNum = 0;
        ArrayList resultAll = instance.getAll();
        for(Object t: resultAll){
            actualRowNum++;
        }
        
        boolean expResult = false;
        boolean result = instance.insertOne(object);
        assertEquals(expResult, result);
        
        resultAll = instance.getAll();
        assertEquals(actualRowNum, resultAll.size());
        
        DeliveryService lastObject = (DeliveryService) resultAll.get(resultAll.size()-1);
        assertEquals(470, lastObject.getStartingPrice(), 0.1);
        assertEquals("delNew", lastObject.getName());
        assertEquals(250, lastObject.getPricePerKilometer(), 0.1);
    }
    
}
