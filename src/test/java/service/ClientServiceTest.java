/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import db.ClientDAO;
import java.util.ArrayList;
import junit.framework.ComparisonFailure;
import model.Client;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author User
 */
public class ClientServiceTest {
    
    public ClientServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of login method, of class ClientService.
     */
    @Test
    public void testLogin() {
        System.out.println("login");
        String username = "nina";
        String password = "buha";
        ClientService instance = new ClientService();
        Client expResult = new Client(1, "nina", "nina", "buha");
        Client result = instance.login(username, password);
        assertEquals(expResult.getUsername(), result.getUsername());
        assertEquals(expResult.getId(), result.getId());
        assertEquals(expResult.getName(), result.getName());
        assertEquals(expResult.getPassword(), result.getPassword());
    }
    
    @Test
    public void testLogin1() {
        System.out.println("login");
        String username = "nina3";
        String password = "buha3";
        ClientService instance = new ClientService();
        Client expResult = new Client(4, "nina3", "nina3", "buha3");
        Client result = instance.login(username, password);
        assertEquals(expResult.getUsername(), result.getUsername());
        assertEquals(expResult.getId(), result.getId());
        assertEquals(expResult.getName(), result.getName());
        assertEquals(expResult.getPassword(), result.getPassword());
    }
    
    // GRESKA
    @Test
    public void testLoginMultipleUsersWithSamePass() {
        System.out.println("login");
        String username = "nina3";
        String password = "buha2";
        ClientService instance = new ClientService();
        Client expResult = new Client(4, "nina3", "nina3", "buha2");
        Client result = instance.login(username, password);
        assertEquals(expResult.getUsername(), result.getUsername());
        assertEquals(expResult.getId(), result.getId());
        assertEquals(expResult.getName(), result.getName());
        assertEquals(expResult.getPassword(), result.getPassword());
    }
    
    // GRESKA - ne proverava lozinku
    @Test
    public void testLoginFails() {
        System.out.println("login");
        String username = "nina";
        String password = "markovic";
        ClientService instance = new ClientService();
        Client result = instance.login(username, password);
        assertNull(result);
    }
    
    // GRESKA - ne proverava username
    @Test
    public void testLoginFails1() {
        System.out.println("login");
        String username = "marko";
        String password = "buha";
        ClientService instance = new ClientService();
        Client result = instance.login(username, password);
        assertNull(result);
    }
    
    @Test(expected = org.junit.ComparisonFailure.class)
    public void testLoginFails2() {
        System.out.println("login");
        String username = "nina2";
        String password = "buha2";
        ClientService instance = new ClientService();
        Client expResult = new Client(4, "nina3", "nina3", "buha3");
        Client result = instance.login(username, password);
        assertEquals(expResult.getUsername(), result.getUsername());
        assertEquals(expResult.getId(), result.getId());
        assertEquals(expResult.getName(), result.getName());
        assertEquals(expResult.getPassword(), result.getPassword());
    }
    
    
    
    
    

    /**
     * Test of register method, of class ClientService.
     */
    
    // GRESKA - 
    @Test
    public void testRegisterUsernameExists() {
        System.out.println("register");
        
        ClientDAO instanceDao = new ClientDAO();
        int actualRowNum = 0;
        ArrayList resultAll = instanceDao.getAll();
        for(Object t: resultAll){
            actualRowNum++;
        }
        
        String name = "ninaa";
        String username = "nina";
        String password = "123";
        ClientService instance = new ClientService();
        boolean expResult = false;
        boolean result = instance.register(name, username, password);
        assertEquals(expResult, result);
        
        Client resultClient = instance.login(username, password);
        assertNull(resultClient);
        
        resultAll = instanceDao.getAll();
        assertEquals(actualRowNum, resultAll.size());
    }
    
    // GRESKA - insertuje iako je prazan username
    @Test
    public void testRegisterUsernameEmpty() {
        System.out.println("register");
        
        ClientDAO instanceDao = new ClientDAO();
        int actualRowNum = 0;
        ArrayList resultAll = instanceDao.getAll();
        for(Object t: resultAll){
            actualRowNum++;
        }
        
        String name = "nina";
        String username = "";
        String password = "123";
        ClientService instance = new ClientService();
        boolean expResult = false;
        boolean result = instance.register(name, username, password);
        assertEquals(expResult, result);
        
        Client resultClient = instance.login(username, password);
        assertNull(resultClient);
        
        resultAll = instanceDao.getAll();
        assertEquals(actualRowNum, resultAll.size());
    }
    
    // GRESKA - insertuje iako je prazan password
    @Test
    public void testRegisterPassEmpty() {
        System.out.println("register");
        
        ClientDAO instanceDao = new ClientDAO();
        int actualRowNum = 0;
        ArrayList resultAll = instanceDao.getAll();
        for(Object t: resultAll){
            actualRowNum++;
        }
        
        String name = "nina33";
        String username = "nina33";
        String password = "";
        ClientService instance = new ClientService();
        boolean expResult = false;
        boolean result = instance.register(name, username, password);
        assertEquals(expResult, result);
        
        Client resultClient = instance.login(username, password);
        assertNull(resultClient);
        
        resultAll = instanceDao.getAll();
        assertEquals(actualRowNum, resultAll.size());
    }
   
    
    // GRESKA - insertuje iako je prazan name
    @Test
    public void testRegisterNameEmpty() {
        System.out.println("register");
        
        ClientDAO instanceDao = new ClientDAO();
        int actualRowNum = 0;
        ArrayList resultAll = instanceDao.getAll();
        for(Object t: resultAll){
            actualRowNum++;
        }
        
        String name = "";
        String username = "cc";
        String password = "cc";
        ClientService instance = new ClientService();
        boolean expResult = false;
        boolean result = instance.register(name, username, password);
        assertEquals(expResult, result);
        
        Client resultClient = instance.login(username, password);
        assertNull(resultClient);
        
        resultAll = instanceDao.getAll();
        assertEquals(actualRowNum, resultAll.size());
    }

    @Test
    public void testRegisterSuccess() {
        System.out.println("register");
        
        ClientDAO instanceDao = new ClientDAO();
        int actualRowNum = 0;
        ArrayList resultAll = instanceDao.getAll();
        for(Object t: resultAll){
            actualRowNum++;
        }
        
        String name = "nina44";
        String username = "nina44";
        String password = "buha44";
        ClientService instance = new ClientService();
        boolean expResult = true;
        boolean result = instance.register(name, username, password);
        assertEquals(expResult, result);
        
        Client expClient = new Client(null, name, username, password);
        Client resultClient = instance.login(username, password);
        assertEquals(expClient.getUsername(), resultClient.getUsername());
        assertEquals(expClient.getName(), resultClient.getName());
        assertEquals(expClient.getPassword(), resultClient.getPassword());
        
        resultAll = instanceDao.getAll();
        assertEquals(actualRowNum+1, resultAll.size());
    }
    
    
    
    
    
    

    /**
     * Test of deleteUser method, of class ClientService.
     */
    @Test
    public void testDeleteUserExists() {
        System.out.println("deleteUser");
        
        ClientDAO instanceDao = new ClientDAO();
        int actualRowNum = 0;
        ArrayList resultAll = instanceDao.getAll();
        for(Object t: resultAll){
            actualRowNum++;
        }
        
        Client c = new Client(3, "nina3", "nina3", "buha3");
        ClientService instance = new ClientService();
        boolean expResult = true;
        boolean result = instance.deleteUser(c);
        assertEquals(expResult, result);
        
        Client resultClient = instance.login("nina3", "buha33");
        assertNull(resultClient);
        
        resultAll = instanceDao.getAll();
        assertEquals(actualRowNum-1, resultAll.size());
    }
    
    @Test
    public void testDeleteUserDoesNotExist() {
        System.out.println("deleteUser");
        
        ClientDAO instanceDao = new ClientDAO();
        int actualRowNum = 0;
        ArrayList resultAll = instanceDao.getAll();
        for(Object t: resultAll){
            actualRowNum++;
        }
        
        Client c = new Client(103, "nnbdjdjj", "hhhhsjsjs", "iisudjdisks");
        ClientService instance = new ClientService();
        boolean expResult = false;
        boolean result = instance.deleteUser(c);
        assertEquals(expResult, result);
        
        Client resultClient = instance.login("hhhhsjsjs", "iisudjdisks");
        assertNull(resultClient);
        
        resultAll = instanceDao.getAll();
        assertEquals(actualRowNum, resultAll.size());
    }

    /**
     * Test of updateInfo method, of class ClientService.
     */
    // GRESKA - ne izmeni name
    @Test
    public void testUpdateInfo() {
        System.out.println("updateInfo");
        Client c = new Client(5, "nina4", "nina4", "buha4");
        String name = "newName";
        String oldPassword = "nina4";
        String password = "newPass";
        ClientService instance = new ClientService();
        instance.updateInfo(c, name, oldPassword, password);
        
        Client expClient = new Client(5, name, c.getUsername(), password);
        Client resultClient = instance.login(c.getUsername(), password);
        assertEquals(expClient.getUsername(), resultClient.getUsername());
        assertEquals(expClient.getName(), resultClient.getName());
        assertEquals(expClient.getPassword(), resultClient.getPassword());
    }
    
    // GRESKA - ne proverava password
    @Test
    public void testUpdateInfoWrongPass() {
        System.out.println("updateInfo");
        Client c = new Client(6, "marija", "maki", "maric");
        ClientDAO instanceDao = new ClientDAO();
        String name = "newMarija";
        String oldPassword = "wrongPass";
        String password = "newMarijaPass";
        ClientService instance = new ClientService();
        instance.updateInfo(c, name, oldPassword, password);
        
        Client expClient = new Client(5, name, c.getUsername(), password);
        Client resultClient = instance.login(c.getUsername(), password);
        
        assertNull(resultClient);
        /*assertEquals(expClient.getUsername(), resultClient.getUsername());
        assertEquals(expClient.getName(), resultClient.getName());
        assertEquals(expClient.getPassword(), resultClient.getPassword());*/
    }
    
}
