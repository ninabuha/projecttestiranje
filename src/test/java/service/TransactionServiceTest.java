/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import db.ClientDAO;
import db.DeliveryServiceDAO;
import db.GenericDAO;
import db.ShopItemDAO;
import db.TransactionDAO;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import model.Client;
import model.DeliveryService;
import model.ShopItem;
import model.Transaction;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.Mockito;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author User
 */
public class TransactionServiceTest {
    
    public TransactionServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of completeTransaction method, of class TransactionService.
     */
    
    // GRESKA - redosled
    @Test
    public void testCompleteTransaction() {
        System.out.println("completeTransaction");
        
        TransactionDAO instanceDao = new TransactionDAO();
        int actualRowNum = 0;
        ArrayList result = instanceDao.getAll();
        for(Object t: result){
            actualRowNum++;
        }
        
        
        int amount = 50;
        float distance = 10.0F;
        ShopItemService ss = new ShopItemService();
        ShopItemDAO instanceDAOSI = new ShopItemDAO();
        ClientDAO instanceDAOCL = new ClientDAO();
        DeliveryServiceDAO instanceDAODS = new DeliveryServiceDAO();
        ShopItem item = instanceDAOSI.getOne(1);
        Client client = instanceDAOCL.getOne(1);
        DeliveryService ds = instanceDAODS.getOne(1);
        int currentAmount = item.getAmount();
        
        TransactionService instance = new TransactionService(ss);
        instance.completeTransaction(client, ds, item, amount, distance);
        
        result = instanceDao.getAll();
        assertEquals(actualRowNum+1, result.size());
        
        
        
        
        if(amount > 20 && amount < 50){
            item.setPrice(item.getPrice()*0.9f);
        }
        else if(amount >= 50){
            item.setPrice(item.getPrice()*0.8f);
        }
        
        float price = ds.getStartingPrice() + item.getPrice();
        float dis = 350/(int)distance*20;
        price -= dis;
        price += (int) distance*100*ds.getPricePerKilometer();
        
        PromotionService ps = spy(PromotionService.class);
        InOrder promotionOrder = inOrder(ps);
        
        float discount = ps.applyPromotions(price, new Date());
        promotionOrder.verify(ps, times(1)).calculateSpecialPromotions();
        promotionOrder.verify(ps, times(1)).checkHolidays(new Date());
        when(ps.checkHolidays(new Date())).thenReturn(true);
        promotionOrder.verify(ps, times(1)).calculateSeasonalPromotions(new Date());
        promotionOrder.verify(ps, times(1)).calculateDiscount();
        
        if(discount <= price/2){
            price -= discount;
        }
        
        
        Transaction newT = new Transaction(null, price, amount, new Date(), client.getId(), client.getId(), ds.getId(), distance);
        Transaction lastObject = (Transaction) result.get(result.size()-1);
        assertEquals(newT.getTotalPrice(), lastObject.getTotalPrice(), 0.1);
        assertEquals(newT.getAmount(), lastObject.getAmount());
        assertEquals(newT.getDate(), lastObject.getDate());
        assertEquals(newT.getClientId(), lastObject.getClientId());
        assertEquals(newT.getShopItemId(), lastObject.getShopItemId());
        assertEquals(newT.getDeliveryServiceId(), lastObject.getDeliveryServiceId());
        assertEquals(newT.getDistance(), lastObject.getDistance(), 0.1);
        
        item = instanceDAOSI.getOne(item.getId());
        assertEquals(currentAmount-amount, item.getAmount());
        
    }
    
    // GRESKA - pogresan exception
    @Test(expected = IllegalArgumentException.class)
    public void testCompleteTransactionInvalidIds() {
        System.out.println("completeTransaction");
        
        TransactionDAO instanceDao = new TransactionDAO();
        int actualRowNum = 0;
        ArrayList result = instanceDao.getAll();
        for(Object t: result){
            actualRowNum++;
        }
        
        
        int amount = 50;
        float distance = 10.0F;
        ShopItemService ss = new ShopItemService();
        ShopItemDAO instanceDAOSI = new ShopItemDAO();
        ClientDAO instanceDAOCL = new ClientDAO();
        DeliveryServiceDAO instanceDAODS = new DeliveryServiceDAO();
        ShopItem item = new ShopItem(null, "majica", 120, 410);
        Client client = new Client(null, "nina", "nina", "buha");
        DeliveryService ds = new DeliveryService(null, "SomeDs", 50, 100);
        int currentAmount = item.getAmount();
        TransactionService instance = new TransactionService(ss);
        instance.completeTransaction(client, ds, item, amount, distance);
        
        result = instanceDao.getAll();
        assertEquals(actualRowNum, result.size());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testCompleteTransactionInvalidDistanceAmount() {
        System.out.println("completeTransaction");
        
        TransactionDAO instanceDao = new TransactionDAO();
        int actualRowNum = 0;
        ArrayList result = instanceDao.getAll();
        for(Object t: result){
            actualRowNum++;
        }
        
        
        int amount = -50;
        float distance = -10.0F;
        ShopItemService ss = new ShopItemService();
        ShopItemDAO instanceDAOSI = new ShopItemDAO();
        ClientDAO instanceDAOCL = new ClientDAO();
        DeliveryServiceDAO instanceDAODS = new DeliveryServiceDAO();
        ShopItem item = new ShopItem(1, "majica", 120, 410);
        Client client = new Client(1, "nina", "nina", "buha");
        DeliveryService ds = new DeliveryService(1, "SomeDs", 50, 100);
        int currentAmount = item.getAmount();
        TransactionService instance = new TransactionService(ss);
        instance.completeTransaction(client, ds, item, amount, distance);
        
        result = instanceDao.getAll();
        assertEquals(actualRowNum, result.size());
    }
    
    
    
    
    
    

    /**
     * Test of getRecentTransactions method, of class TransactionService.
     */
    
    // GRESKA
    @Test
    public void testGetRecentTransactions() {
        System.out.println("getRecentTransactions");
        ShopItemService si = new ShopItemService();
        TransactionService instance = new TransactionService(si);
        ArrayList<Transaction> expResult = new ArrayList<>();
        ArrayList<Transaction> result = instance.getRecentTransactions();
        
        TransactionDAO insatnceDao = new TransactionDAO();
        ArrayList<Transaction> allTransactions = new ArrayList<>();
        allTransactions = insatnceDao.getAll();
        
        LocalDate localDate = LocalDate.now().minusDays(30);
        Date date = java.sql.Date.valueOf(localDate);
        for(Transaction t: allTransactions){
            if(t.getDate().after(date)){
                expResult.add(t);
            }
        }
        assertEquals(expResult.size(), result.size());
    }
    
}
