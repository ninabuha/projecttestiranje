/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import db.ShopItemDAO;
import db.TransactionDAO;
import java.time.LocalDate;
import java.time.Period;
import static java.time.temporal.TemporalQueries.localDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import model.ShopItem;
import model.Transaction;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;

/**
 *
 * @author User
 */
public class ShopItemServiceTest {
    
    public ShopItemServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of postItem method, of class ShopItemService.
     */
    @Test
    public void testPostItemSuccess() {
        System.out.println("postItem");
        
        ShopItemDAO instanceDao = new ShopItemDAO();
        int actualRowNum = 0;
        ArrayList resultAll = instanceDao.getAll();
        for(Object t: resultAll){
            actualRowNum++;
        }
        
        String name = "NewItem";
        float price = 200.0F;
        int amount = 40;
        ShopItemService instance = new ShopItemService();
        boolean expResult = true;
        boolean result = instance.postItem(name, price, amount);
        assertEquals(expResult, result);
        
        resultAll = instanceDao.getAll();
        assertEquals(actualRowNum+1, resultAll.size());
        
        ShopItem newObj = new ShopItem(null, name, price, amount);
        ShopItem lastObject = (ShopItem) resultAll.get(resultAll.size()-1);
        assertEquals(newObj.getName(), lastObject.getName());
        assertEquals(newObj.getPrice(), lastObject.getPrice(), 1.0);
        assertEquals(newObj.getAmount(), lastObject.getAmount());
    }
    
    @Test
    public void testPostItemEmptyName() {
        System.out.println("postItem");
        
        ShopItemDAO instanceDao = new ShopItemDAO();
        int actualRowNum = 0;
        ArrayList resultAll = instanceDao.getAll();
        for(Object t: resultAll){
            actualRowNum++;
        }
        
        String name = "";
        float price = 216.0F;
        int amount = 46;
        ShopItemService instance = new ShopItemService();
        boolean expResult = false;
        boolean result = instance.postItem(name, price, amount);
        assertEquals(expResult, result);
        
        resultAll = instanceDao.getAll();
        assertEquals(actualRowNum, resultAll.size());
    }
    
    @Test
    public void testPostItemPriceAndAmountZero() {
        System.out.println("postItem");
    
        ShopItemDAO instanceDao = new ShopItemDAO();
        int actualRowNum = 0;
        ArrayList resultAll = instanceDao.getAll();
        for(Object t: resultAll){
            actualRowNum++;
        }
        
        String name = "SomeItem";
        float price = 0.0F;
        int amount = 0;
        ShopItemService instance = new ShopItemService();
        boolean expResult = true;
        boolean result = instance.postItem(name, price, amount);
        assertEquals(expResult, result);
    
        resultAll = instanceDao.getAll();
        assertEquals(actualRowNum+1, resultAll.size());
        
        ShopItem newObj = new ShopItem(null, name, price, amount);
        ShopItem lastObject = (ShopItem) resultAll.get(resultAll.size()-1);
        assertEquals(newObj.getName(), lastObject.getName());
        assertEquals(newObj.getPrice(), lastObject.getPrice(), 1.0);
        assertEquals(newObj.getAmount(), lastObject.getAmount());
    }
    
    @Test
    public void testPostItemPriceAndAmountNegative() {
        System.out.println("postItem");
        
        ShopItemDAO instanceDao = new ShopItemDAO();
        int actualRowNum = 0;
        ArrayList resultAll = instanceDao.getAll();
        for(Object t: resultAll){
            actualRowNum++;
        }
        
        String name = "SomeItemNegative";
        float price = -200.0F;
        int amount = -4;
        ShopItemService instance = new ShopItemService();
        boolean expResult = false;
        boolean result = instance.postItem(name, price, amount);
        assertEquals(expResult, result);
        
        resultAll = instanceDao.getAll();
        assertEquals(actualRowNum, resultAll.size());
    }

    
    
    
    
    /**
     * Test of removeItem method, of class ShopItemService.
     */
    @Test
    public void testRemoveItemSuccess() {
        System.out.println("removeItem");
        
        ShopItemDAO instanceDao = new ShopItemDAO();
        int actualRowNum = 0;
        ArrayList resultAll = instanceDao.getAll();
        for(Object t: resultAll){
            actualRowNum++;
        }
        
        ShopItem s = new ShopItem(120, "sal", 600, 16);
        ShopItemService instance = new ShopItemService();
        boolean expResult = true;
        boolean result = instance.removeItem(s);
        assertEquals(expResult, result);
        
        ArrayList resultDao = instanceDao.getAll();
        assertEquals(actualRowNum-1, resultDao.size());
    }
    
    // GRESKA - vraca true za invalid id /// MOZDA NIJE GRESKA!!!!!!!!!!!
    @Test
    public void testRemoveItemInvalidId() {
        System.out.println("removeItem");
        
        ShopItemDAO instanceDao = new ShopItemDAO();
        int actualRowNum = 0;
        ArrayList resultAll = instanceDao.getAll();
        for(Object t: resultAll){
            actualRowNum++;
        }
        
        ShopItem s = new ShopItem(400, "fggg", 600, 16);
        ShopItemService instance = new ShopItemService();
        boolean expResult = false;
        boolean result = instance.removeItem(s);
        assertEquals(expResult, result);
        
        ArrayList resultDao = instanceDao.getAll();
        assertEquals(actualRowNum, resultDao.size());
    }
    
    @Test
    public void testRemoveItemNullId() {
        System.out.println("removeItem");
         
        ShopItemDAO instanceDao = new ShopItemDAO();
        int actualRowNum = 0;
        ArrayList resultAll = instanceDao.getAll();
        for(Object t: resultAll){
            actualRowNum++;
        }
        
        ShopItem s = new ShopItem(null, "fggg", 600, 16);
        ShopItemService instance = new ShopItemService();
        boolean expResult = false;
        boolean result = instance.removeItem(s);
        assertEquals(expResult, result);
        
        ArrayList resultDao = instanceDao.getAll();
        assertEquals(actualRowNum, resultDao.size());
    }

    /**
     * Test of buy method, of class ShopItemService.
     */
    
    // GRESKA
    @Test
    public void testBuySuccess() {
        System.out.println("buy");
        int id = 1;
        ShopItemDAO instanceDao = new ShopItemDAO();
        ShopItem s = instanceDao.getOne(id);
        int amount = 2;
        ShopItemService instance = new ShopItemService();
        instance.buy(s, amount);
        
        ShopItem actualObject = instanceDao.getOne(id);
        assertEquals(s.getAmount()-amount, actualObject.getAmount());
    }
    
    @Test
    public void testBuyNegativeAmount() {
        System.out.println("buy");
        int id = 1;
        ShopItemDAO instanceDao = new ShopItemDAO();
        ShopItem s = instanceDao.getOne(id);
        int amount = -2;
        ShopItemService instance = new ShopItemService();
        instance.buy(s, amount);
        
        ShopItem actualObject = instanceDao.getOne(id);
        assertEquals(s.getAmount(), actualObject.getAmount());
    }
    
    @Test
    public void testBuyAmountZero() {
        System.out.println("buy");
        int id = 1;
        ShopItemDAO instanceDao = new ShopItemDAO();
        ShopItem s = instanceDao.getOne(id);
        int amount = 0;
        ShopItemService instance = new ShopItemService();
        instance.buy(s, amount);
        
        ShopItem actualObject = instanceDao.getOne(s.getId());
        assertEquals(s.getAmount(), actualObject.getAmount());
    }
    
    @Test(expected = NullPointerException.class)
    public void testBuyNullId() {
        System.out.println("buy");
        ShopItem s = new ShopItem(null, "majica", 1200, 250);
        int amount = 0;
        ShopItemService instance = new ShopItemService();
        instance.buy(s, amount);
        
        ShopItemDAO instanceDao = new ShopItemDAO();
        ShopItem actualObject = instanceDao.getOne(s.getId());
        assertEquals(s.getAmount(), actualObject.getAmount());
    }
    
    @Test
    public void testBuyMoreAmount() {
        System.out.println("buy");
        int id = 3;
        ShopItemDAO instanceDao = new ShopItemDAO();
        ShopItem s = instanceDao.getOne(id);
        int currentAmount = s.getAmount();
        int amount = 10000;
        ShopItemService instance = new ShopItemService();
        instance.buy(s, amount);
        
        ShopItem actualObject = instanceDao.getOne(s.getId());
        assertEquals(currentAmount, actualObject.getAmount());
    }

    /**
     * Test of stockUp method, of class ShopItemService.
     */
    @Test
    public void testStockUp() {
        System.out.println("stockUp");
        int id = 105;
        ShopItemDAO instanceDao = new ShopItemDAO();
        ShopItem s = instanceDao.getOne(id);
        int amount = 10;
        int amBeforeSU = s.getAmount() + amount;
        ShopItemService instance = new ShopItemService();
        instance.stockUp(s, amount);
        
        ShopItem actualObject = instanceDao.getOne(id);
        assertEquals(amBeforeSU, actualObject.getAmount());
    }
    
    @Test
    public void testStockUpNegativeAmount() {
        System.out.println("stockUp");
        int id = 105;
        ShopItemDAO instanceDao = new ShopItemDAO();
        ShopItem s = instanceDao.getOne(id);
        int currentAmount = s.getAmount();
        int amount = -10;
        ShopItemService instance = new ShopItemService();
        instance.stockUp(s, amount);
        
        ShopItem actualObject = instanceDao.getOne(s.getId());
        assertEquals(currentAmount, actualObject.getAmount());
    }

    
    
    
    
    /**
     * Test of checkIfPopular method, of class ShopItemService.
     */
    @Test
    public void testCheckIfPopular() {
        System.out.println("checkIfPopular");
        int id = 1;
        ShopItemDAO instanceDao = new ShopItemDAO();
        ShopItem s = instanceDao.getOne(id);
        ShopItemService instance = new ShopItemService();
        boolean expResult = false;
        boolean result = instance.checkIfPopular(s);
        
        TransactionDAO instanceDaoT = new TransactionDAO();
        ArrayList<Transaction> allTransactions = instanceDaoT.getAll();
        
       
        LocalDate localDate = LocalDate.now().minusDays(30);
        Date date = java.sql.Date.valueOf(localDate);
        
            
        int amountIn30Days = 0;
        for(Transaction t: allTransactions){
            if(t.getShopItemId() == s.getId()){
                if(t.getDate().after(date) || t.getDate().equals(date)){
                    amountIn30Days += t.getAmount();
                }
            }
        }
        if((s.getPrice() > 300) && (amountIn30Days > s.getAmount()*0.6) || (s.getPrice() < 300) && (amountIn30Days > s.getAmount()*0.8)){
            expResult = true;
        }
        else{
            expResult = false;
        }
        
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCheckIfPopularNot() {
        System.out.println("checkIfPopular");
        int id = 2;
        ShopItemDAO instanceDao = new ShopItemDAO();
        ShopItem s = instanceDao.getOne(id);
        ShopItemService instance = new ShopItemService();
        boolean expResult = false;
        boolean result = instance.checkIfPopular(s);
        
        
        TransactionDAO instanceDaoT = new TransactionDAO();
        ArrayList<Transaction> allTransactions = instanceDaoT.getAll();
        
       
        LocalDate localDate = LocalDate.now().minusDays(30);
        Date date = java.sql.Date.valueOf(localDate);
        
            
        int amountIn30Days = 0;
        for(Transaction t: allTransactions){
            if(t.getShopItemId() == s.getId()){
                if(t.getDate().after(date) || t.getDate().equals(date)){
                    amountIn30Days += t.getAmount();
                }
            }
        }
        if((s.getPrice() > 300) && (amountIn30Days > s.getAmount()*0.6) || (s.getPrice() < 300) && (amountIn30Days > s.getAmount()*0.8)){
            expResult = true;
        }
        else{
            expResult = false;
        }
        
        assertEquals(expResult, result);
    }
    
    
    
    
    /**
     * Test of getTrendingIndex method, of class ShopItemService.
     */
    
    // GRESKA - ne vraca dobar rezultat
    @Test
    public void testGetTrendingIndex() {
        System.out.println("getTrendingIndex");
        int id = 3;
        ShopItemDAO instanceDao = new ShopItemDAO();
        ShopItem s = instanceDao.getOne(id);
        ShopItemService instance = new ShopItemService();
        float expResult = 0.0F;
        float result = instance.getTrendingIndex(s);
        
        TransactionDAO instanceDaoT = new TransactionDAO();
        ArrayList<Transaction> allTransactions = instanceDaoT.getAll();
        
       
        Date lastDate = new Date();
        
            
        float profit = 0;
        lastDate = allTransactions.get(0).getDate();
        for(Transaction t: allTransactions){
            if(t.getShopItemId() == s.getId()){
                if(t.getDate().after(lastDate)){
                    lastDate = t.getDate();
                    System.out.println(lastDate);
                }
                if(t.getShopItemId() == s.getId()){
                    profit += t.getTotalPrice();
                }
            }
        }
        Date curentDate = new Date();
        long diffInMillies = Math.abs(curentDate.getTime() - lastDate.getTime());
        long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
        int numOfDays = (int) diff;
        System.out.println("INT: "+ numOfDays);
        expResult = profit/numOfDays;
        
        assertEquals(expResult, result, 0.1);
    }
    
    @Test
    public void testGetTrendingIndexIdInOneTransaction() {
        System.out.println("getTrendingIndex");
        int id = 10;
        ShopItemDAO instanceDao = new ShopItemDAO();
        ShopItem s = instanceDao.getOne(id);
        ShopItemService instance = new ShopItemService();
        float expResult = 50.0F;
        float result = instance.getTrendingIndex(s);
        
        TransactionDAO instanceDaoT = new TransactionDAO();
        ArrayList<Transaction> allTransactions = instanceDaoT.getAll();
        
       
        Date lastDate = new Date();
        
            
        float profit = 0;
        lastDate = allTransactions.get(0).getDate();
        for(Transaction t: allTransactions){
            if(t.getShopItemId() == s.getId()){
                if(t.getDate().after(lastDate)){
                    lastDate = t.getDate();
                    System.out.println(lastDate);
                }
                if(t.getShopItemId() == s.getId()){
                    profit += t.getTotalPrice();
                }
            }
        }
        Date curentDate = new Date();
        long diffInMillies = Math.abs(curentDate.getTime() - lastDate.getTime());
        long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
        int numOfDays = (int) diff;
        System.out.println("INT: "+ numOfDays);
        expResult = profit/numOfDays;
        
        assertEquals(expResult, result, 0.1);
    }
    
    @Test
    public void testGetTrendingIndexIdInZeroTransactions() {
        System.out.println("getTrendingIndex");
        int id = 222;
        ShopItemDAO instanceDao = new ShopItemDAO();
        ShopItem s = instanceDao.getOne(id);
        ShopItemService instance = new ShopItemService();
        float expResult = 0.0F;
        float result = instance.getTrendingIndex(s);
        
        
        TransactionDAO instanceDaoT = new TransactionDAO();
        ArrayList<Transaction> allTransactions = instanceDaoT.getAll();
        
       
        Date lastDate = new Date();
        
            
        float profit = 0;
        lastDate = allTransactions.get(0).getDate();
        for(Transaction t: allTransactions){
            if(t.getShopItemId() == s.getId()){
                if(t.getDate().after(lastDate)){
                    lastDate = t.getDate();
                    System.out.println(lastDate);
                }
                if(t.getShopItemId() == s.getId()){
                    profit += t.getTotalPrice();
                }
            }
        }
        Date curentDate = new Date();
        long diffInMillies = Math.abs(curentDate.getTime() - lastDate.getTime());
        long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
        int numOfDays = (int) diff;
        System.out.println("INT: "+ numOfDays);
        expResult = profit/numOfDays;
        
        assertEquals(expResult, result, 0.1);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testGetTrendingIndexInvalidaId() {
        System.out.println("getTrendingIndex");
        ShopItem s = new ShopItem(null, "ddd", 100, 3);;
        ShopItemService instance = new ShopItemService();
        float expResult = 50.0F;
        float result = instance.getTrendingIndex(s);
        
        TransactionDAO instanceDao = new TransactionDAO();
        ArrayList<Transaction> allTransactions = instanceDao.getAll();
        
       
        Date lastDate = new Date();
        
            
        float profit = 0;
        lastDate = allTransactions.get(0).getDate();
        for(Transaction t: allTransactions){
            if(t.getShopItemId() == s.getId()){
                if(t.getDate().after(lastDate)){
                    lastDate = t.getDate();
                    System.out.println(lastDate);
                }
                if(t.getShopItemId() == s.getId()){
                    profit += t.getTotalPrice();
                }
            }
        }
        Date curentDate = new Date();
        long diffInMillies = Math.abs(curentDate.getTime() - lastDate.getTime());
        long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
        int numOfDays = (int) diff;
        System.out.println("INT: "+ numOfDays);
        expResult = profit/numOfDays;
        
        assertEquals(expResult, result, 0.1);
    }
    
}
