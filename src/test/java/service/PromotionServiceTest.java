/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

/**
 *
 * @author User
 */
public class PromotionServiceTest {
    
    public PromotionServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of calculateSeasonalPromotions method, of class PromotionService.
     */
    /**
     * Test of checkHolidays method, of class PromotionService.
     */
    @Test
    public void testCheckHolidays() {
        System.out.println("checkHolidays");
        Date d = new Date();
        PromotionService spy = spy(PromotionService.class);
        boolean expResult = true;
        when(spy.checkHolidays(d)).thenReturn(true);
        
        boolean result = spy.checkHolidays(d);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCheckHolidays1() {
        System.out.println("checkHolidays");
        Date d = new Date();
        PromotionService spy = spy(PromotionService.class);
        boolean expResult = false;
        when(spy.checkHolidays(d)).thenReturn(false);
        
        boolean result = spy.checkHolidays(d);
        assertEquals(expResult, result);
    }

    /**
     * Test of calculateSpecialPromotions method, of class PromotionService.
     */

    /**
     * Test of calculateDiscount method, of class PromotionService.
     */
    /**
     * Test of applyPromotions method, of class PromotionService.
     */
    @Test
    public void testApplyPromotions() {
        System.out.println("applyPromotions");
        float price = 100.0F;
        Date d = null;
        PromotionService spy = spy(PromotionService.class);
        float expResult = 5000.0F;
        
        when(spy.applyPromotions(price, d)).thenReturn(5000.0f);
        assertEquals(expResult, spy.applyPromotions(price, d), 0.1);
    }
    
}
