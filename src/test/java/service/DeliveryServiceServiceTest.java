/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import db.DeliveryServiceDAO;
import java.util.ArrayList;
import model.DeliveryService;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author User
 */
public class DeliveryServiceServiceTest {
    
    public DeliveryServiceServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of register method, of class DeliveryServiceService.
     */
    @Test
    public void testRegisterSuccess() {
        System.out.println("register");
        
        DeliveryServiceDAO instanceDao = new DeliveryServiceDAO();
        int actualRowNum = 0;
        ArrayList resultAll = instanceDao.getAll();
        for(Object t: resultAll){
            actualRowNum++;
        }
        
        String name = "SomeDS";
        float pricePerKilometer = 50.0F;
        float startingPrice = 100.0F;
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = true;
        boolean result = instance.register(name, pricePerKilometer, startingPrice);
        assertEquals(expResult, result);
        
        resultAll = instanceDao.getAll();
        assertEquals(actualRowNum+1, resultAll.size());
        
        DeliveryService newDs = new DeliveryService(null, name, pricePerKilometer, startingPrice);
        DeliveryService lastObject = (DeliveryService) resultAll.get(resultAll.size()-1);
        assertEquals(newDs.getName(), lastObject.getName());
        assertEquals(newDs.getStartingPrice(), lastObject.getStartingPrice(), 0.1);
        assertEquals(newDs.getPricePerKilometer(), lastObject.getPricePerKilometer(), 0.1);
    }
    
    @Test
    public void testRegisterEmptyName() {
        System.out.println("register");
        
        DeliveryServiceDAO instanceDao = new DeliveryServiceDAO();
        int actualRowNum = 0;
        ArrayList resultAll = instanceDao.getAll();
        for(Object t: resultAll){
            actualRowNum++;
        }
        
        String name = "";
        float pricePerKilometer = 50.0F;
        float startingPrice = 100.0F;
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = false;
        boolean result = instance.register(name, pricePerKilometer, startingPrice);
        assertEquals(expResult, result);
        
        resultAll = instanceDao.getAll();
        assertEquals(actualRowNum, resultAll.size());
    }
    
    @Test
    public void testRegisterPriceZero() {
        System.out.println("register");
        
        DeliveryServiceDAO instanceDao = new DeliveryServiceDAO();
        int actualRowNum = 0;
        ArrayList resultAll = instanceDao.getAll();
        for(Object t: resultAll){
            actualRowNum++;
        }
        
        String name = "NameDS";
        float pricePerKilometer = 0.0F;
        float startingPrice = 100.0F;
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = false;
        boolean result = instance.register(name, pricePerKilometer, startingPrice);
        assertEquals(expResult, result);
        
        resultAll = instanceDao.getAll();
        assertEquals(actualRowNum, resultAll.size());;
    }
    
    @Test
    public void testRegisterStartingPriceZero() {
        System.out.println("register");
        
        DeliveryServiceDAO instanceDao = new DeliveryServiceDAO();
        int actualRowNum = 0;
        ArrayList resultAll = instanceDao.getAll();
        for(Object t: resultAll){
            actualRowNum++;
        }
        
        String name = "NameDS";
        float pricePerKilometer = 50.0F;
        float startingPrice = 0.0F;
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = false;
        boolean result = instance.register(name, pricePerKilometer, startingPrice);
        assertEquals(expResult, result);
        
        resultAll = instanceDao.getAll();
        assertEquals(actualRowNum, resultAll.size());
    }

    
    
    
    /**
     * Test of deleteDeliveryService method, of class DeliveryServiceService.
     */
    @Test
    public void testDeleteDeliveryService() {
        System.out.println("deleteDeliveryService");
        
        DeliveryServiceDAO instanceDao = new DeliveryServiceDAO();
        int actualRowNum = 0;
        ArrayList resultAll = instanceDao.getAll();
        for(Object t: resultAll){
            actualRowNum++;
        }
        
        DeliveryService d = new DeliveryService(4, "SomeDS", 50, 100);
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = true;
        boolean result = instance.deleteDeliveryService(d);
        assertEquals(expResult, result);
        
        resultAll = instanceDao.getAll();
        assertEquals(actualRowNum-1, resultAll.size());
    }
    
    // GRESKA - treba da vrati false, jer ne postoji ds sa tim id
    @Test
    public void testDeleteDeliveryServiceFails() {
        System.out.println("deleteDeliveryService");
        
        DeliveryServiceDAO instanceDao = new DeliveryServiceDAO();
        int actualRowNum = 0;
        ArrayList resultAll = instanceDao.getAll();
        for(Object t: resultAll){
            actualRowNum++;
        }
        
        DeliveryService d = new DeliveryService(400, "SomeDS", 50, 100);
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = false;
        boolean result = instance.deleteDeliveryService(d);
        assertEquals(expResult, result);
        
        resultAll = instanceDao.getAll();
        assertEquals(actualRowNum, resultAll.size());
    }

    
    
    
    
    /**
     * Test of updateInfo method, of class DeliveryServiceService.
     */
    @Test
    public void testUpdateInfoSuccess() {
        System.out.println("updateInfo");
        DeliveryService d = new DeliveryService(4, "SomeDS", 50, 100);
        String name = "NewName";
        float startingPrice = 250.0F;
        float pricePerKilometer = 400.0F;
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = true;
        boolean result = instance.updateInfo(d, name, startingPrice, pricePerKilometer);
        assertEquals(expResult, result);
        
        DeliveryServiceDAO instanceDao = new DeliveryServiceDAO();
        DeliveryService actualObject = instanceDao.getOne(d.getId());
        assertEquals(name, actualObject.getName());
        assertEquals(startingPrice, actualObject.getStartingPrice(), 1.0);
        assertEquals(pricePerKilometer, actualObject.getPricePerKilometer(), 1.0);
    }
    
    // GRESKA - prazno polje name
    @Test
    public void testUpdateInfoEmptyNewName() {
        System.out.println("updateInfo");
        DeliveryService d = new DeliveryService(4, "NewName", 250, 400);
        String name = "";
        float startingPrice = 255.0F;
        float pricePerKilometer = 36.0F;
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = false;
        boolean result = instance.updateInfo(d, name, startingPrice, pricePerKilometer);
        assertEquals(expResult, result);
        
        DeliveryServiceDAO instanceDao = new DeliveryServiceDAO();
        DeliveryService actualObject = instanceDao.getOne(d.getId());
        assertEquals(d.getName(), actualObject.getName());
        assertEquals(d.getStartingPrice(), actualObject.getStartingPrice(), 1.0);
        assertEquals(d.getPricePerKilometer(), actualObject.getPricePerKilometer(), 1.0);
    }
    
    // GRESKA - price 0
    @Test
    public void testUpdateInfoPriceZero() {
        System.out.println("updateInfo");
        DeliveryService d = new DeliveryService(4, "NewName", 250, 400);
        String name = "Neww";
        float startingPrice = 144.0F;
        float pricePerKilometer = 0.0F;
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = false;
        boolean result = instance.updateInfo(d, name, startingPrice, pricePerKilometer);
        assertEquals(expResult, result);
        
        DeliveryServiceDAO instanceDao = new DeliveryServiceDAO();
        DeliveryService actualObject = instanceDao.getOne(d.getId());
        assertEquals(d.getName(), actualObject.getName());
        assertEquals(d.getStartingPrice(), actualObject.getStartingPrice(), 1.0);
        assertEquals(d.getPricePerKilometer(), actualObject.getPricePerKilometer(), 1.0);
    }
    
    // GRESKA - sp 0
    @Test
    public void testUpdateInfoSPriceZero() {
        System.out.println("updateInfo");
        DeliveryService d = new DeliveryService(4, "NewName", 250, 400);
        String name = "Neww";
        float startingPrice = 0.0F;
        float pricePerKilometer = 44.0F;
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = false;
        boolean result = instance.updateInfo(d, name, startingPrice, pricePerKilometer);
        assertEquals(expResult, result);
        
        DeliveryServiceDAO instanceDao = new DeliveryServiceDAO();
        DeliveryService actualObject = instanceDao.getOne(d.getId());
        assertEquals(d.getName(), actualObject.getName());
        assertEquals(d.getStartingPrice(), actualObject.getStartingPrice(), 1.0);
        assertEquals(d.getPricePerKilometer(), actualObject.getPricePerKilometer(), 1.0);
    }
    
    // GRESKA - negativne cene
    @Test
    public void testUpdateInfoNegativePrices() {
        System.out.println("updateInfo");
        DeliveryService d = new DeliveryService(3, "NewName", 250, 400);
        String name = "NovDS";
        float startingPrice = -144.0F;
        float pricePerKilometer = -120.0F;
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = false;
        boolean result = instance.updateInfo(d, name, startingPrice, pricePerKilometer);
        assertEquals(expResult, result);
        
        DeliveryServiceDAO instanceDao = new DeliveryServiceDAO();
        DeliveryService actualObject = instanceDao.getOne(d.getId());
        assertEquals(d.getName(), actualObject.getName());
        assertEquals(d.getStartingPrice(), actualObject.getStartingPrice(), 1.0);
        assertEquals(d.getPricePerKilometer(), actualObject.getPricePerKilometer(), 1.0);
    }
    
    // GRESKA - nepostojeci id
    @Test
    public void testUpdateInfoInvalidId() {
        System.out.println("updateInfo");
        DeliveryService d = new DeliveryService(400, "NewName", 250, 400);
        String name = "fgggg";
        float startingPrice = 1000.0F;
        float pricePerKilometer = 4544.0F;
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = false;
        boolean result = instance.updateInfo(d, name, startingPrice, pricePerKilometer);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testUpdateInfoIdNull() {
        System.out.println("updateInfo");
        DeliveryService d = new DeliveryService(null, "NewName", 250, 400);
        String name = "fgggg";
        float startingPrice = 1000.0F;
        float pricePerKilometer = 4544.0F;
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = false;
        boolean result = instance.updateInfo(d, name, startingPrice, pricePerKilometer);
        assertEquals(expResult, result);
    }
    
}
